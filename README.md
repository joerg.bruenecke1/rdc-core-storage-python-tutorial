# RDC-CORE-Storage-Python-Tutorial

Simple first-steps tutorial for accessing the RDC CORE Storage

## Introduction

This tutorial's purpose is to get people started interacting with the NFDI RDC CORE Storage, an instance of the [ScienceObjectsDB](https://github.com/ScienceObjectsDB).

After finishing the tutorial users with basic prior knowledge of [Python](https://www.python.org/) should be able to authenticate against and use the CORE Storage web API for uploading and downloading data objects.

Setting up a Python environment is beyond the scope of the tutorial.

The API used in the examples below is a service in development state. **Do not rely on stability and data persistence.**

### Tools

#### BioData DB website

Interaction with the API via a functionally restricted graphical user interface can be conducted via the [BioData DB website](https://website.scienceobjectsdb.nfdi-dev.gi.denbi.de/login). Registering a user account is a basic access requirement (as to how, see below). 

Logging in to the website is necessary at least once to create an API access token.

#### Swagger UI
The web API's endpoints can be inspected at the projects [Swagger UI](https://gateway.scienceobjectsdb.nfdi-dev.gi.denbi.de/swagger-ui/) without prior registration or login.

### User Account

Authentication is coupled to a [Github](https://github.com/) user account. To get your Github account registered for the CORE Storage API go to the [BioData DB website](https://website.scienceobjectsdb.nfdi-dev.gi.denbi.de/login) and click the login button.

![screenshot of BioDataDB website login button](img/screenshot_BioDataDB_website_login_button.png)

Below the credential fields in the opening dialogue find and click the button labeled GitHub.

![screenshot of BioDataDB website login form](img/screenshot_BioDataDB_website_login_form.png)

Follow Github's instructions and after successfull login send an e-mail to Marius Diekmann (Marius.Dieckmann [at] computational.bio.uni-giessen.de) containing your Github account name to request access to the CORE Storage API.

### Authentication

Once an account is registered, your first step should be a login into the [BioData DB website](http://website.scienceobjectsdb.nfdi-dev.gi.denbi.de/). Use your Github account unless told otherwise.

Interacting with the API in the scope of this tutorial requires at least a project id and an API token associated with a project. Consequently, the first steps after logging in are the creation of a project, obtaining the project id and minting an API key.

#### Project setup
For all further activities create a test project by clicking the button labeled `New Project`. Under `Specs` add a project name and a description, which are the mandatory information. Play with `Metadata` and `Labels`, there is no harm in it.

![screenshot of BioDataDB website project creation dialogue](img/screenshot_BioDataDB_website_Create_Project.png)

Clicking the button labeled `Generate` creates the project. Afterwards the new project can be found in the project list.

Click the symbols in the `Actions` column to see what they do. Later on you will need the project id, which can be copied by clicking on the first symbol. Try it out and paste the id into your favourite editor. You should see a [long string](https://en.wikipedia.org/wiki/Universally_unique_identifier) cronstructed out of digits, letters and dashes. Memorize how to obtain this project id for later.

![screenshot of BioDataDB project list](img/screenshot_BioDataDB_website_project_list.png)

#### Creating API access keys
Click on the key symbol for your recently created project. In the dialogue titled `API-Keys - ...` click the plus-button to generate a new API access token.

In the list of tokens, in the `Actions` column find and click the button that copies the API token. This token will be needed in all further interations with the API. It is the secret that has to be presented instead of your username and password.

![screenshot of BioDataDB API key dialogue](img/screenshot_BioDataDB_website_API_keys.png)

API keys in this list enable the bearer to manipulate the associated project. __It is crucial that you keep the API keys secret__ unless you have a good reason not to and take measures to prevent exploitation.

Memorize where and how to obtain the API key for your project in case you need it in the following API interactions.

### First API interaction

This is the first place, where you can have a meaningful interation with the API. Remember that you had to create a project in order to create an API access key? You will need the project's id and the API key in order to communication with the API using Python. In case you forgot where you can find both, go back a couple of steps to the [Authentication](#authentication) section.

Start your Python shell and import a couple of modules. We need some tools to parse [JSON](https://en.wikipedia.org/wiki/JSON) and to talk to HTTP servers.

```python
$ python
Python 3.10.5 (main, Jun  9 2022, 00:00:00) [GCC 11.3.1 20220421 (Red Hat 11.3.1-2)] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>> import requests # so you can talk HTTP
```

Errors at this point are most probably due to a missing `requests` package. If you do not know how to install such a package try an internet search including your operating system and the missing module as search terms. Finding out how to use Python's package manager `pip` is also worth trying. A quick solution might be to run

```
pip install requests
```
on the command line (not in the Python session).

If importing `requests` is successful, you can go on and define some variables for re-use in later steps. Find the project id and the api key (reminder: keep it secret, unlike me) at the according places in the BioDataDB website (see previous steps). Replace both in the following variable assignments.

```python
>>> project_id = "abcdefgh-bdb4-422e-bd28-eeb687ace306" # replace with your own
>>> api_key = "5TmnLmrvsQsC5viAaS5abcdefghabcdefghabcdefghu8mx2TpHd1pB/4jE8" # replace with your own
```

After storing project id and api key in variables, also assign the API's URL to a variable and construct the [HTTP request header](https://developer.mozilla.org/en-US/docs/Glossary/Request_header). You do not need to know the details about the latter. However, it is necessary for authentication and, to that end, must be included in requests against the API.

```python
>>> api_version = "1" # might change in future
>>> api_url = "https://gateway.scienceobjectsdb.nfdi-dev.gi.denbi.de/api/v" + api_version # url of testing instance
>>> request_headers = {"Content-Type":"application/json", "Grpc-Metadata-API_TOKEN":api_key} # used for authentication
```

At this point you are set for the first API requests. Let's see if you can get some information about the project. In order to know, which URL you have to query, go to the [Swagger UI](https://gateway.scienceobjectsdb.nfdi-dev.gi.denbi.de/swagger-ui/) and try to find a useful endpoint. `/api/v1/project/{id}` looks promising. After clicking on the item you get the list of input parameters - it is only `id` in this case, which has be provided as part of the URL path. This means you have to query the API url, which was stored in the variable `$api_url` before, and you have to add `/api/v1/project/{id}` where `{id}` must be replaced with a proper project id (one of which was stored in the variable `project_id` earlier).


![screenshot of Swagger UI Get Project](img/screenshot_swagger_ui_get_project.png)

Store the whole URL in the variable `api_get_project_url` and then make a get-request at the API. In order to be able to access the API's response later on, the response is stored in the variable with the name `response`.

```python
>>> api_get_project_url = api_url + "/project/" + project_id # assemble request URL
>>> response = requests.get(api_get_project_url, headers = request_headers) # make request and store result in variable
```

Did it work? You can find out by looking at the response.

```python
>>> response.ok # returns False if problems occurred.
True
```

Yes, it did. What is in the response?

```python
>>> response.json() # print the response
{'project': {'id': 'abcdefgh-bdb4-422e-bd28-eeb687ace306', 'name': 'mytestproject', 'description': "It's the best project.", 'labels': [], 'annotations': [], 'users': [{'userId': 'abcdefgh-0c9b-4a04-a13b-062d6e82d7bf', 'rights': [], 'resource': 'RESOURCE_UNSPECIFIED'}], 'bucket': '', 'status': 'STATUS_AVAILABLE', 'stats': {'objectCount': '0', 'objectGroupCount': '0', 'accSize': '-1', 'avgObjectSize': -1, 'userCount': '1'}}}
```

This looks familiar. The value of the field `name` contains the project name set at project creation in the BioDataDB website. The same is true for the project description. Other fields tell you that there are no objects stored in the project and that only one user is assigned to it.


### Creating a dataset

After establishing basic API interaction the next step is setting up project structure. There are several entities to accomplish this, one of which is a thing called dataset (the term may change in the future). Think of a dataset as something like a directory which holds data objects.

You can create datasets via the website and through an API endpoint using Python.

#### Creating a dataset using the website
The BioDataDB website, which we used to setup a project and to create an API key, can be used to create datasets and to upload objects. To create such a dataset (think folder/directory in your project) go to the project list, and in the `Actions` column find and click on the right-arrow symbol to open the project context.

![screenshot of BioDataDB project list](img/screenshot_BioDataDB_website_gotoproject.png)

Click `New Dataset` and fill in at least name and description, then click `Generate`.

![screenshot of BioDataDB Create Dataset dialogue](img/screenshot_BioDataDB_website_create_dataset.png)

Just like a project, a dataset has an id. You can copy the id by clicking the first symbol in the `Action` column of the dataset table. Try it out and paste the id in your favourite editor to find out that it is a long string constructed from letters, digits, and dashes, again.

#### Listing datasets using python

As a next step try to obtain information about the [just created dataset](#creating-a-dataset-using-the-website). If you have not done so already, import the `requets` module and store the necessary information in variables - project id, API access key, API url.

```python
$ python
Python 3.10.5 (main, Jun  9 2022, 00:00:00) [GCC 11.3.1 20220421 (Red Hat 11.3.1-2)] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>> import requests # so you can talk HTTP
>>> project_id = "abcdefgh-bdb4-422e-bd28-eeb687ace306" # replace with your own
>>> api_key = "5TmnLmrvsQsC5viAaS5abcdefghabcdefghabcdefghu8mx2TpHd1pB/4jE8" # replace with your own
>>> api_version = "1" # might change in future
>>> api_url = "https://gateway.scienceobjectsdb.nfdi-dev.gi.denbi.de/api/v" + api_version # url of testing instance
>>> request_headers = {"Content-Type":"application/json", "Grpc-Metadata-API_TOKEN":api_key} # used for authentication
```

Since you are dealing with datasets now, you need a different endpoint than before. In the [Swagger UI](https://gateway.scienceobjectsdb.nfdi-dev.gi.denbi.de/swagger-ui/#/ProjectService/ProjectService_GetProjectDatasets) you will find the endpoint `/api/v1/project/{id}/projectdatasets`. Closer investigation tells you, there is only one parameter to submit - the `{id}` of the project. The full url can be assembled like this:

```python
>>> api_get_project_datasets_url = api_url + "/project/" + project_id + "/projectdatasets"
```

Now try the actual request.

```python
>>> response = requests.get(api_get_project_datasets_url, headers = request_headers)

```

If the request was succesful, the result should resemble something similar to the following:

```python
>>> response.ok # check if the request was successful
True
>>> response.json() # print the response
{'datasets': [{'id': 'abcdefgh-c048-4538-9a6c-e70aa9e49126', 'name': 'myfirstdataset', 'description': "It's the best dataset.", 'created': '2022-06-23T12:21:16.719488Z', 'labels': [], 'annotations': [], 'projectId': 'abcdefgh-bdb4-422e-bd28-eeb687ace306', 'isPublic': False, 'status': 'STATUS_AVAILABLE', 'bucket': 'test-bucket-0-abcdefgh-c048-4538-9a6c-e70aa9e49126', 'stats': {'objectCount': '0', 'objectGroupCount': '0', 'accSize': '0', 'avgObjectSize': 0}, 'metadataObjects': []}]}
```

In the response, you should be able to find the dataset you created via the website

#### Creating datasets using Python

In order to create a dataset using Python, you have to setup all the variables as in the previous steps. If you did not quit your Python session you probably do not have to repeat the following. It does no harm, though.

```python
$ python
Python 3.10.5 (main, Jun  9 2022, 00:00:00) [GCC 11.3.1 20220421 (Red Hat 11.3.1-2)] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>> import requests # so you can talk HTTP
>>> project_id = "abcdefgh-bdb4-422e-bd28-eeb687ace306" # replace with your own
>>> api_key = "5TmnLmrvsQsC5viAaS5abcdefghabcdefghabcdefghu8mx2TpHd1pB/4jE8" # replace with your own
>>> api_version = "1" # might change in future
>>> api_url = "https://gateway.scienceobjectsdb.nfdi-dev.gi.denbi.de/api/v" + api_version # url of testing instance
>>> request_headers = {"Content-Type":"application/json", "Grpc-Metadata-API_TOKEN":api_key} # used for authentication
```

Now to the dataset-creation-specific steps. In the [Swagger UI](https://gateway.scienceobjectsdb.nfdi-dev.gi.denbi.de/swagger-ui/#/DatasetService/DatasetService_CreateDataset) you can find the endpoint `/api/v1/dataset/create`. It uses the [HTTP POST method](https://en.wikipedia.org/wiki/POST_(HTTP)), which is different from before. As a consequence the parameter submission happens in a different way. Before, when you used GET requests, the parameters were part of the URL. With POST requests the parameters are submitted in the _request body_. Just like the request header the request body can be assembled as a [Python dictionary](https://www.w3schools.com/python/python_dictionaries.asp). First store the name and description of the dataset you want to create in variables, then use the variables when constructing the request body.

```python
>>> api_create_dataset_url = api_url + "/dataset/create" # assemble request url
>>> dataset_name = "my second dataset" # set dataset name
>>> dataset_description = "It was created using Python." # set dataset description
>>> request_body = {"name": dataset_name, "projectId": project_id, "description": dataset_description, "labels": [], "metadata": []} # build request body
```

How do you know which variables you can and have to set? You can look those up in the [Swagger UI](https://gateway.scienceobjectsdb.nfdi-dev.gi.denbi.de/swagger-ui/) - it provides an example of the expected data structure next to the keyword `body`, when you click on the endpoint `/api/v1/dataset/create`.

After you have created the request body, you have to include it into your request. As before, it is a good idea to store the response in a variable.

```python
response = requests.post(api_create_dataset_url, json=request_body, headers = request_headers)
>>> response.ok # check if the request was successful
True
>>> response.json() # print the response
{'id': 'abcdefgh-41c3-48d6-a1ef-929ebde62ff2'}
```

The response is ok but less verbose this time. It consists of the id of the newly created dataset. You can use the method from section [Listing datasets using python](#listing_datasets_using_python) to see if the just created dataset is part of your project:

```python
>>> response = requests.get(api_get_project_datasets_url, headers = request_headers) # this is from an earlier section
>>> response.json() # print the response
{'datasets': [{'id': 'abcdefgh-41c3-48d6-a1ef-929ebde62ff2', 'name': 'my second dataset', 'description': 'It was created using Python.', 'created': '2022-06-24T11:27:31.494143Z', 'labels': [], 'annotations': [], 'projectId': 'abcdefgh-bdb4-422e-bd28-eeb687ace306', 'isPublic': False, 'status': 'STATUS_AVAILABLE', 'bucket': 'test-bucket-0-abcdefgh-41c3-48d6-a1ef-929ebde62ff2', 'stats': {'objectCount': '0', 'objectGroupCount': '0', 'accSize': '0', 'avgObjectSize': 0}, 'metadataObjects': []}, {'id': 'abcdefgh-c048-4538-9a6c-e70aa9e49126', 'name': 'myfirstdataset', 'description': "It's the best dataset.", 'created': '2022-06-23T12:21:16.719488Z', 'labels': [], 'annotations': [], 'projectId': 'abcdefgh-bdb4-422e-bd28-eeb687ace306', 'isPublic': False, 'status': 'STATUS_AVAILABLE', 'bucket': 'test-bucket-0-abcdefgh-c048-4538-9a6c-e70aa9e49126', 'stats': {'objectCount': '0', 'objectGroupCount': '0', 'accSize': '0', 'avgObjectSize': 0}, 'metadataObjects': []}]}
```

Yes! 

Your ids will differ. However, the project contains two datasets - one created via the website, one created using Python.

### Uploading objects

It is time to upload some data objects. The following two sections show how to do this via the website and via Python.

#### Uploading objects via the BioDataDB website

In the website's project overview (just reload the website if you do not know how to get there) find your project and in the actions column click the right-arrow button.

![screenshot of BioDataDB project list](img/screenshot_BioDataDB_website_project_list2.png)

You should enter the dataset overview with the datasets you created in the past. Pick the first dataset you created and click the right-arrow in the actions column.

![screenshot of BioDataDB dataset overview](img/screenshot_BioDataDB_website_dataset_overview.png)

You should enter the object groups overview. Presently object groups are intended to be a general purpose grouping feature. They might vanish in future or get named differently. For the time being creation of object groups is necessary to upload data objects via the ObjectsDB Website.

Create an new object group by clicking the button labeled `New Object Group`. This triggers the `Create Object Group` dialogue. Click on `Specs` and fill in the `Name` and `Description` of the object group. 

![screenshot of BioDataDB create object group dialogue](img/screenshot_BioDataDB_create_object_group.png)

To select the data object to upload click `Data Objects`, then `Add Object`.

![screenshot of BioDataDB add object dialogue](img/screenshot_BioDataDB_website_add_object.png)

In the opening `Add Object` dialogue click on `Specs`, then on the up-arrow upload button.

![screenshot of BioDataDB add object dialogue](img/screenshot_BioDataDB_add_object2.png)

Pick your favourite data file in the opening file picking dialogue. I created a file named `hello.txt` whith the content `Hello, World!`. Feel free to be more creative.

Upon selection of the file, you should find that the `Filename` and `Filetype` fields in the `Add Object` dialogue have changed magically. The name and extension should match your just selected file.

Click the button `Add to Object Group`. The `Add Object` dilalogue closes and you should see your data file added to the `Data Objects` list. Feel free to have fun and repeat the steps to add more objects.

Clicking `Create Object Group` finishes the Object Group creation and finalizes the data object upload.

![screenshot of BioDataDB create object group dialogue with object](img/screenshot_BioDataDB_create_object_group2.png)

**REMARK: At the time of writing the website malfunctions and actually does not accept the upload. Please use the Python method described in the next section.**

#### Uploading objects - The Python method

You probably know the basics by now. You have to import module for http handling and set some variables if you have not done so in your python shell.

```python
>>> import requests # so you can talk HTTP
>>> project_id = "abcdefgh-bdb4-422e-bd28-eeb687ace306" # replace with your own
>>> api_key = "5TmnLmrvsQsC5viAaS5abcdefghabcdefghabcdefghu8mx2TpHd1pB/4jE8" # replace with your own
>>> api_version = "1" # might change in future
>>> api_url = "https://gateway.scienceobjectsdb.nfdi-dev.gi.denbi.de/api/v" + api_version # url of testing instance
>>> request_headers = {"Content-Type":"application/json", "Grpc-Metadata-API_TOKEN":api_key} # used for authentication
```

Uploading an object to the CORE Storage actually does not require the existence of an object group as implied by the section descibing object upload via the website. In the [Swagger UI](https://gateway.scienceobjectsdb.nfdi-dev.gi.denbi.de/swagger-ui/) you can find the endpoint `/api/v1/object` under `DatasetObjectsService`. Set a variable holding this endpoint.

```python
>>> api_prepare_object_upload_url = api_url + "/object"
```


This endpoint can be used to prepare the upload of an object. Be aware, that the endpoint is not for the actual upload. The process goes like this: first you prepare the upload by submitting some metadata - such as dataset id, filename, extension, file size, labels, ... - then, if sucessfull, the API's response will be an upload link. The link is valid for some time an must be used to upload the actual data object. 

The http method used to prepare the object upload is a `POST` again. As before, this means you have to create a request body containing the mandatory parameters, of which there are several that you can find in the example request body in the Swagger UI. However, you can start with a bare minimum of filename, file extension and the id of the dataset holding the object. The latter you can find via the website - go to the dataset overview and copy the dataset id. Alternatively list the datasets and find the correct id via the Python method explored in an earlier section. 

```python
>>> request_body = {"filename": "hello2","filetype": "txt","datasetId": "abcdefgh-41c3-48d6-a1ef-929ebde62ff2"} # build request body
>>> response = requests.post(api_prepare_object_upload_url, json=request_body, headers = request_headers)
```

Did it work?

```python
>>> response.ok
True
>>> response.json()
{'id': 'a8e820f6-746b-435d-a5d5-abcdefgh', 'uploadLink': 'https://test-bucket-0-abcdefgh-41c3-48d6-a1ef-929ebde62ff2.s3.computational.bio.uni-giessen.de/abcdefgh-bdb4-422e-bd28-eeb687ace306/abcdefgh-41c3-48d6-a1ef-929ebde62ff2/a8e820f6-746b-435d-a5d5-abcdefgh/hello2?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=1891c4e9baeb40a8a2c57aa3c8495c82%2F20220629%2F%2Fs3%2Faws4_request&X-Amz-Date=20220629T093013Z&X-Amz-Expires=900&X-Amz-SignedHeaders=host&x-id=PutObject&X-Amz-Signature=a586cb6c15ff45f73c80fb02ed54c295596770636f8ce63312fba3ff10012a59'}
```

This looks good. The response contains an `id` and an `uploadLink` for the object you want to store in the CORE Storage. Extract the link into a variable for easy use in the upload step. Also, create a variable holding the object id. You will need it soon, when you will try to verify the successful upload.

```python
>>> object_upload_link = response.json()['uploadLink']
>>> object_id = response.json()['id']
```

Now that you have an upload link, you can put a file into the CORE Storage. You will have to use the `HTTP PUT` method - so the request looks a tiny bit different than before. Also, you should have prepared an object (i. e. a file) and know the absolute path to the file. In the following example the file is `/tmp/hello.txt`. Replace this path with the one that points to the file you want to upload. 

```python
>>> local_file_path = '/tmp/hello.txt' # replace with your own
>>> response = requests.put(object_upload_link, data = open(local_file_path, 'rb'), headers = request_headers)
```

Note the `open(local_file_path, 'rb')` part. It is where the local file is opened and read for being uploaded. If you get an error, it is probably because the file path is incorrect. The `r` in `rb` stands for read-only, the `b` for binary mode. More on Python's `open()` function can be found in the [Python documentation](https://docs.python.org/3/library/functions.html#open), but is not part of this tutorial.

Success should coincide with a friendly `True` and a status code of `200`:

```python
>>> response.ok
True
>>> response.status_code
200
```

The CORE Storage now holds the uploaded data object. It is not ready for being downloaded, though. First, the object(-upload) must be finalized. The API provides an endpoint for exactly this, which in the [Swagger UI](https://gateway.scienceobjectsdb.nfdi-dev.gi.denbi.de/swagger-ui/) you can find under: `/api/v1/object/finish`

```python
>>> api_object_finish_url = api_url + '/object/finish'
>>> response = requests.post(api_object_finish_url, json={"id": object_id}, headers = request_headers)
>>> response.ok
True
```

Where does the `object_id` variable come from? You assigned the value a few steps prior, after you requested the upload link.

Is the object actually there? Can you download the object? Find out in the next section.

### Downloading Objects

While uploading objects to the CORE Storage is a satisfying enough activity, you probably also want to be able to get your objects back. If you know an object's id - and you just used one for finishing an upload - you can request it. Well, actually not. It is like with the upload - you first request a link which you can then use for downloading the object into a file.

```python
>>> api_get_object_downloadlink_url = api_url + "/objectload/download/" + object_id
>>> response = requests.get(api_get_object_downloadlink_url, headers = request_headers)
>>> response.ok
True
>>> object_download_link = response.json()['downloadLink']
```

The variable `object_download_link` now holds a URL which can be used to download the data object. It looks something like that:

```python
>>> response.json()['downloadLink']
'https://test-bucket-0-abcdefgh-41c3-48d6-a1ef-929ebde62ff2.s3.computational.bio.uni-giessen.de/abcdefgh-bdb4-422e-bd28-eeb687ace306/abcdefgh-41c3-48d6-a1ef-929ebde62ff2/a8e820f6-746b-435d-a5d5-abcdefgh/hello2?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=1891c4e9baeb40a8a2c57aa3c8495c82%2F20220701%2F%2Fs3%2Faws4_request&X-Amz-Date=20220701T133717Z&X-Amz-Expires=900&X-Amz-SignedHeaders=host&x-id=GetObject&X-Amz-Signature=d3f86852eb608f79e9530b6386f94b912d391fc4fd01d03bc27da3c513360ab3'
```

You can copy the link and paste it into your browser's address bar and download a file with the object's content. You can also use Python to request the file and print it on your display like so:


```python
>>> download_link = response.json()['downloadLink'] # store the downloadlink into a variable
>>> requests.get(download_link).text # get the data and print as text
'Hello, World!\n'
```

The output should resemble what you uploaded earlier.

You can also save the the content into a file. Make sure you pick one that does not exist, so that no data is destroyed. Pick for instance `/tmp/thisfiledoesnotexist.txt` and use the `download_link` from before.

```python
>>> local_output_file = '/tmp/thisfiledoesnotexist.txt' # pick your own
>>>> with open(local_output_file, 'w') as f: # press enter here
...   f.write(requests.get(download_link).text)  # type two spaces to begin the line and press enter at the end
...  # press enter again 
14
```

The `14` is the number of bytes written. It is the return value of the `open` function, which in this case opens a file in `w` (as in write) mode. Try to find the output file on your computer and open it. It should contain what you uploaded earlier.

This is all fine, when you just have the object id around. But what do you do, if you don't? You list the objects stored in a dataset, the id of which you will have to find out first. You can do that via the website or the Python method discussed earlier. Assuming that you have found out the dataset id you can list a dataset's objects by sending a request to the API endpoint `/api/v1/datasetobjects/get`.

```python
>>> dataset_id = 'abcdefgh-41c3-48d6-a1ef-929ebde62ff2' # replace with your own dataset id
>>> api_dataset_objectlist_url = api_url + "/datasetobjects/get"
>>> response = requests.post(api_dataset_objectlist_url, json={"id": dataset_id}, headers = request_headers)
>>> response.ok
True
>>> response.json()
{'objects': [{'id': 'a8e820f6-746b-435d-a5d5-abcdefgh', 'filename': 'hello2', 'filetype': 'txt', 'labels': [], 'annotations': [], 'created': '2022-06-29T09:30:13.749626Z', 'locations': [{'objectLocation': {'id': '3a3b5f09-3703-453b-9b5d-b9a9b09bb1c7', 'bucket': 'test-bucket-0-abcdefgh-41c3-48d6-a1ef-929ebde62ff2', 'key': 'abcdefgh-bdb4-422e-bd28-eeb687ace306/abcdefgh-41c3-48d6-a1ef-929ebde62ff2/a8e820f6-746b-435d-a5d5-abcdefgh/hello2', 'url': 'https://s3.computational.bio.uni-giessen.de', 'status': 'STATUS_INITIATING', 'uploadId': ''}}, {'objectLocation': {'id': '8eac216e-aef3-42d1-8bbc-ffe0a911648b', 'bucket': 'test-bucket-0-abcdefgh-41c3-48d6-a1ef-929ebde62ff2', 'key': 'abcdefgh-bdb4-422e-bd28-eeb687ace306/abcdefgh-41c3-48d6-a1ef-929ebde62ff2/a8e820f6-746b-435d-a5d5-abcdefgh/hello2', 'url': 'https://s3.computational.bio.uni-giessen.de', 'status': 'STATUS_INITIATING', 'uploadId': ''}}], 'defaultLocation': {'objectLocation': {'id': '8eac216e-aef3-42d1-8bbc-ffe0a911648b', 'bucket': 'test-bucket-0-abcdefgh-41c3-48d6-a1ef-929ebde62ff2', 'key': 'abcdefgh-bdb4-422e-bd28-eeb687ace306/abcdefgh-41c3-48d6-a1ef-929ebde62ff2/a8e820f6-746b-435d-a5d5-abcdefgh/hello2', 'url': 'https://s3.computational.bio.uni-giessen.de', 'status': 'STATUS_INITIATING', 'uploadId': ''}}, 'origin': None, 'contentLen': '0', 'generated': None, 'objectGroupId': '', 'datasetId': 'abcdefgh-41c3-48d6-a1ef-929ebde62ff2', 'projectId': 'abcdefgh-bdb4-422e-bd28-eeb687ace306', 'status': 'STATUS_STAGING', 'stats': None, 'fileformat': ''}, {'id': 'c1a7513a-89c8-4bc0-8e19-817175362a13', 'filename': 'hello2', 'filetype': 'txt', 'labels': [], 'annotations': [], 'created': '2022-07-01T11:17:03.429093Z', 'locations': [{'objectLocation': {'id': '4bbec651-bd5c-433b-9840-6478e4b1dfa6', 'bucket': 'test-bucket-0-abcdefgh-41c3-48d6-a1ef-929ebde62ff2', 'key': 'abcdefgh-bdb4-422e-bd28-eeb687ace306/abcdefgh-41c3-48d6-a1ef-929ebde62ff2/c1a7513a-89c8-4bc0-8e19-817175362a13/hello2', 'url': 'https://s3.computational.bio.uni-giessen.de', 'status': 'STATUS_INITIATING', 'uploadId': ''}}, {'objectLocation': {'id': 'a04d2b78-d8fb-40cb-b3ed-23398bd52475', 'bucket': 'test-bucket-0-abcdefgh-41c3-48d6-a1ef-929ebde62ff2', 'key': 'abcdefgh-bdb4-422e-bd28-eeb687ace306/abcdefgh-41c3-48d6-a1ef-929ebde62ff2/c1a7513a-89c8-4bc0-8e19-817175362a13/hello2', 'url': 'https://s3.computational.bio.uni-giessen.de', 'status': 'STATUS_INITIATING', 'uploadId': ''}}], 'defaultLocation': {'objectLocation': {'id': 'a04d2b78-d8fb-40cb-b3ed-23398bd52475', 'bucket': 'test-bucket-0-abcdefgh-41c3-48d6-a1ef-929ebde62ff2', 'key': 'abcdefgh-bdb4-422e-bd28-eeb687ace306/abcdefgh-41c3-48d6-a1ef-929ebde62ff2/c1a7513a-89c8-4bc0-8e19-817175362a13/hello2', 'url': 'https://s3.computational.bio.uni-giessen.de', 'status': 'STATUS_INITIATING', 'uploadId': ''}}, 'origin': None, 'contentLen': '0', 'generated': None, 'objectGroupId': '', 'datasetId': 'abcdefgh-41c3-48d6-a1ef-929ebde62ff2', 'projectId': 'abcdefgh-bdb4-422e-bd28-eeb687ace306', 'status': 'STATUS_STAGING', 'stats': None, 'fileformat': ''}]}
```

It looks ugly, but the objects are there - look for the filename you used in the upload and go a couple of characters back and you will find an object id. Extracting one into a variable would look like this:

```python
>>> object_id = response.json()['objects'][0]['id'] # extract the id of the first object in the list of objects
```


## What's next?

There is plenty you have learnt regarding the interaction with the CORE Storage API using Python up to here. However, depending on your use case you might want to further explore its capabilities. A good start is always the [Swagger UI](https://gateway.scienceobjectsdb.nfdi-dev.gi.denbi.de/swagger-ui/#/). It lists the existing API endpoints, documents the mandatory and optional request parameters and shows exemplary successful replies. You can even interact with the API if you provide an API key in the dialogue that opens when you click `Authorize` at the top of the Swagger UI homepage.

Compiling Python dictionaries following the Swagger UI examples should help to get you along quiet far. Using the Swagger UI and observing the network traffic in your browser's developer tools might help to reveal the differences between the successful Swagger UI request and your unsuccessful request via Python.

## License
The content of this tutorial can be used and distributed according to the [Beerware License](https://en.wikipedia.org/wiki/Beerware).

